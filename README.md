 Microsservice responsible ccomposing primitive event types, one for each capability.
 
 The new primitive eventtypes are only registered when they are needed for 
 processing derived event types, registered by the cep-cataloger microservice.
 
 The event-composer is also responsible for converting data from 
 the actuator-controller, in real time, and convert into events, represented in 
 Avro Schemas.