# Use official Maven's alpine image as base
FROM maven:3.6-jdk-13

#Environment Variables
ENV REDIS_HOSTNAME redis-cep-cataloger


RUN mkdir -p /event-composer/
ADD . /event-composer/
WORKDIR /event-composer/


RUN mvn package

RUN ls target/


#Compilation
#CMD "java -jar target/event-composer-0.0.1-SNAPSHOT.jar"
