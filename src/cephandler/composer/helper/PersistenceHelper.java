package cephandler.composer.helper;

import cephandler.utils.InterscityEventType;
import cephandler.database.Store;

public class PersistenceHelper {
    private String redisHost;
    private Store store;

    public PersistenceHelper(String redisHost){
        this.redisHost = redisHost;
        this.store = new Store(redisHost);
    }



    public void saveNewEvent(InterscityEventType interscityEventType){
        System.out.println("Persist into redis for cep-work: "+interscityEventType.getCepEventUuid());
        this.store.setEventTypeName(interscityEventType.getCepEventUuid(),interscityEventType.getEventName());
        this.store.setAvroSchema(interscityEventType.getCepEventUuid(),interscityEventType.getAvroSchema());

    }

    public String getRedisHost() {
        return redisHost;
    }

}
